package com.equiron.dpp.common.model.transit;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.validator.ExistDbCatalog;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product")
public class Product {
    @Documentation("Артикул товара")
    @MinOccurs(1)
    @Facets(pattern = "(\\d){14}")
    @XmlElement(required = true)
    @ExistDbCatalog
    private String ntin;
    
    @NotNull
    @Documentation("Порядковый номер товара, подлежащего прослеживаемости, в соответствии с сопроводительным документом")
    private Integer accompDocProductNum;
    
    @MinOccurs(1)
    @Documentation("Регистрационный номер декларации на товары, в соответствии с которой товары, подлежащие прослеживаемости, помещены под таможенную процедуру выпуска для внутреннего потребления")
    @XmlElement(required = true)
    private String declarationNum;
    
    @MinOccurs(0)
    @Documentation("Порядковый номер товара, подлежащего прослеживаемости, в соответствии с декларацией на товары")
    private Integer declarationProductNum;
    
    @XmlElement(required = true)
    private Units units;
    
    @Documentation("Количество товара в указанных единицах измерения")
    @XmlElement(required = true)
    private Integer quantity;
    
    @Documentation("Вид прослеживаемости")
    @XmlElement(required = true, name = "")
    @Facets(pattern = "физическая|документальная")
    private String trackingType;
    
    @XmlElementWrapper(required = true)
    @XmlElements({
    @XmlElement(name = "box",  required = true, type = Box.class),
    @XmlElement(name = "item", required = true, type = Item.class) })
    private List<HasSerial> boxes;

    public Product() {
    }

    public Product(String ntin, Integer accompDocProductNum, String declarationNum, Integer declarationProductNum,
                   Units units, Integer quantity, String trackingType, List<HasSerial> items) {
        this.ntin = ntin;
        this.accompDocProductNum = accompDocProductNum;
        this.declarationNum = declarationNum;
        this.declarationProductNum = declarationProductNum;
        this.units = units;
        this.quantity = quantity;
        this.trackingType = trackingType;
        this.boxes = items;
    }

    public String getNtin() {
        return ntin;
    }

    public void setNtin(String ntin) {
        this.ntin = ntin;
    }

    public Integer getAccompDocProductNum() {
        return accompDocProductNum;
    }

    public void setAccompDocProductNum(Integer accompDocProductNum) {
        this.accompDocProductNum = accompDocProductNum;
    }

    public String getDeclarationNum() {
        return declarationNum;
    }

    public void setDeclarationNum(String declarationNum) {
        this.declarationNum = declarationNum;
    }

    public Integer getDeclarationProductNum() {
        return declarationProductNum;
    }

    public void setDeclarationProductNum(Integer declarationProductNum) {
        this.declarationProductNum = declarationProductNum;
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getTrackingType() {
        return trackingType;
    }

    public void setTrackingType(String trackingType2) {
        this.trackingType = trackingType2;
    }

    public List<HasSerial> getItems() {
        return boxes;
    }

    @SuppressWarnings("unchecked")
    public void setItems(List<? extends HasSerial> items) {
        this.boxes = (List<HasSerial>) items;
    }
}
