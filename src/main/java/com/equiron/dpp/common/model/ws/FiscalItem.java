package com.equiron.dpp.common.model.ws;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Товар")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FiscalItem")
public class FiscalItem {
    @Documentation("Название товара")
    @XmlElement(required = false)
    private String name;

    @Documentation("Цена")
    @XmlElement(required = true)
    @NotNull
    private BigDecimal price;

    @Documentation("Количество")
    @XmlElement(required = true)
    @NotNull
    private BigDecimal quantity;

    @Documentation("Код продукта")
    @XmlElement(required = true)
    @NotEmpty
    private String productCode;

    @Documentation("Сумма")
    @XmlElement(required = false)
    private BigDecimal sum;

    @Documentation("Ставка НДС")
    @XmlElement(required = false)
    @MinOccurs(0)
    private int nds;

    @Documentation("Сумма НДС")
    @XmlElement(required = false)
    private BigDecimal ndsSum;

    public FiscalItem() {
        // empty
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public int getNds() {
        return nds;
    }

    public void setNds(int nds) {
        this.nds = nds;
    }

    public BigDecimal getNdsSum() {
        return ndsSum;
    }

    public void setNdsSum(BigDecimal ndsSum) {
        this.ndsSum = ndsSum;
    }

}
