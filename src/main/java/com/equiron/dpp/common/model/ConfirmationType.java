package com.equiron.dpp.common.model;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmationType")
@Documentation("Статус документа")
public enum ConfirmationType {
    @XmlTransient
    DPP_DELIVERED,
    
    @Documentation("Документ создан")
    CREATED,

    @Documentation("Документ принят")
    SYNC_DELIVERED,
    
    @Documentation("НСПТ подтвердила получение документа")
    NTS_DELIVERED, 
    
    @Documentation("НСПТ приняла документ")
    NTS_ACCEPTED,
    
    @Documentation("НСПТ отклонила документ")
    NTS_REJECTED,
    
    @Documentation("НСПТ подтверждает получение успешно обработанного документа")
    ACCEPTANCE_DELIVERED,
    
    @Documentation("НСПТ подтверждает получение отклоненного документа")
    REJECTION_DELIVERED,
    
//    @Documentation("")
//    ENDPOINT_DELIVERED,
//    
//    @Documentation("")
//    ENDPOINT_ACCEPTED,
//    
//    @Documentation("")
//    ENDPOINT_REJECTED, 

    @Documentation("Ошибка валидации документа")
    VALIDATION_ERROR,
    
    @Documentation("Отказ в принятии в реестр участника товарооборота")
    PARTICIPANT_REJECTED,

    @Documentation("Документ получен участником товарооборота")
    PARTICIPANT_DELIVERED,

    @Documentation("Принятие в реестр участника товарооборота")
    PARTICIPANT_ACCEPTANCE

}
