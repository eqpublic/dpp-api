package com.equiron.dpp.common.model;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@Documentation("Вид атрибута из списка")
@XmlEnum
//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public enum ProductStatusUpdate {
    @XmlEnumValue("Черновик")               DRAFT("Черновик"),
    @XmlEnumValue("Опубликован")            PUBLISH("Опубликован"),
    @XmlEnumValue("Отправлен")         		SEND("Отправлен"),
    @XmlEnumValue("Подтвержден")   			CONFIRM("Подтвержден"); 
//    @XmlEnumValue("Удален")                 DELETED("Удален");
    
    private final String value;
 
    ProductStatusUpdate(String v) {
        value = v;
    }
 
    public String value() {
        return value;
    }
 
    public static ProductStatusUpdate fromValue(String v) {
        for (ProductStatusUpdate c: ProductStatusUpdate.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
 
}