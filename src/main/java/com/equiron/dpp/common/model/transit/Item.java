package com.equiron.dpp.common.model.transit;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.transit.HasSerial;

@Documentation("Единица товара")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Item")
public class Item extends HasSerial {
    @Documentation("Максимальная цена")
    private long maxPrice;

    public long getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(long maxPrice) {
        this.maxPrice = maxPrice;
    }
}
