package com.equiron.dpp.common.model.entity;

import java.util.List;
import java.util.stream.Collectors;

public class ParticipantDto {
	private long since;

	private String uid;
	
	private String id;

	private String name;

	private String country;

	private String taxpayerId;

	private String regDate;

	private String unregDate;
	
	private String extended;

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSince() {
        return since;
    }

    public void setSince(long since) {
        this.since = since;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTaxpayerId() {
        return taxpayerId;
    }

    public void setTaxpayerId(String taxpayerId) {
        this.taxpayerId = taxpayerId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getUnregDate() {
        return unregDate;
    }

    public void setUnregDate(String unregDate) {
        this.unregDate = unregDate;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public static ParticipantDto assemble(Participant participant) {
		ParticipantDto dto = new ParticipantDto();

		dto.id = participant.getId();
		dto.uid = participant.getUid();
		dto.country = participant.getCountry();
		dto.name = participant.getName();
		dto.taxpayerId = participant.getTaxpayerId();
		dto.regDate = participant.getRegDate();
		dto.unregDate = participant.getUnregDate();
		dto.extended = participant.getExtended();

		return dto;
	}

	public static List<ParticipantDto> assemble(List<Participant> list) {
		return list.stream().map(ParticipantDto::assemble).collect(Collectors.toList());
	}
}
