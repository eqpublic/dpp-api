package com.equiron.dpp.common.model.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Фискальные документы")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FiscalDocument")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class FiscalDocument extends TrackedDocument {
//    @Documentation("Список фискальных документов")
//    @XmlElementWrapper
//    @XmlElement(name = "fiscal", required = true)
//    private List<FiscalReceipt> fiscalReceipts;
    
//    @XmlElement(required = false)
//    @Documentation("Метка времени записи документа в БД")
//    private long since;

//    public List<FiscalReceipt> getFiscalReceipts() {
//        return fiscalReceipts;
//    }
//
//    public void setFiscalReceipts(List<FiscalReceipt> fiscalReceipts) {
//        this.fiscalReceipts = fiscalReceipts;
//    }

//    public long getSince() {
//        return since;
//    }
//
//    public void setSince(long since) {
//        this.since = since;
//    }
}
