package com.equiron.dpp.common.model.transit;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonValue;

@XmlEnum(String.class)
@XmlType(name="")
@Documentation("Единицы изменения товара\n"
        + "<!-- КИЛОГРАММ  |   КГ\n" + 
        "СТО ШТУК    |   100 ШТ\n" + 
        "1000 КИЛОВАТТ-ЧАС   |   1000 КВТ*Ч\n" + 
        "1000 ЛИТРОВ |   1000 Л\n" + 
        "1000 КУБИЧЕСКИХ МЕТРОВ  |   1000 М3\n" + 
        "ТЫСЯЧА ШТУК |   1000 ШТ\n" + 
        "ГРАММ   |   Г\n" + 
        "ГРАММ ДЕЛЯЩИХСЯ ИЗОТОПОВ    |   Г Д/И\n" + 
        "МЕТРИЧЕСКИЙ КАРАТ(1КАРАТ=2*10(-4)КГ |   КАР\n" + 
        "КИЛОГРАММ СУХОГО НА 90 % ВЕЩЕСТВА   |   КГ 90% С/В\n" + 
        "КИЛОГРАММ ПЕРОКСИДА ВОДОРОДА    |   КГ H2O2\n" + 
        "КИЛОГРАММ ОКСИДА КАЛИЯ  |   КГ K2O\n" + 
        "КИЛОГРАММ ГИДРОКСИДА КАЛИЯ  |   КГ KOH\n" + 
        "КИЛОГРАММ АЗОТА |   КГ N\n" + 
        "КИЛОГРАММ ГИДРОКСИДА НАТРИЯ |   КГ NAOH\n" + 
        "КИЛОГРАММ ПЯТИОКИСИ ФОСФОРА |   КГ P2O5\n" + 
        "КИЛОГРАММ УРАНА |   КГ U\n" + 
        "КЮРИ    |   КИ\n" + 
        "ЛИТР    |   Л\n" + 
        "ЛИТР ЧИСТОГО (100%) СПИРТА  |   Л 100% СПИРТА\n" + 
        "МЕТР    |   М\n" + 
        "КВАДРАТНЫЙ МЕТР |   М2\n" + 
        "КУБИЧЕСКИЙ МЕТР |   М3\n" + 
        "ПАРА    |   ПАР\n" + 
        "ГРУЗОПОДЪЕМНОСТЬ В ТОННАХ   |   Т ГРП\n" + 
        "ШТУКА   |   ШТ -->")
public enum Units {
    @XmlEnumValue("КГ")             KG("КГ"),
    @XmlEnumValue("100 ШТ")         HUNDRED_PS("100 ШТ"),
    @XmlEnumValue("1000 КВТ*Ч")     THOUSAND_KWT_H("1000 КВТ*Ч"),
    @XmlEnumValue("1000 Л")         THOUSAND_L("1000 Л"),
    @XmlEnumValue("1000 М3")        THOUSAND_M3("1000 М3"),
    @XmlEnumValue("1000 ШТ")        THOUSAND_PS("1000 ШТ"),
    @XmlEnumValue("Г")              GRAMM("Г"),
    @XmlEnumValue("Г Д/И")          GRAMM_DI("Г Д/И"),
    @XmlEnumValue("КАР")            KARAT("КАР"),
    @XmlEnumValue("КГ 90% С/В")     KG_DRY("КГ 90% С/В"),
    @XmlEnumValue("КГ H2O2")        KG_HYDROGEN_PEROXIDE("КГ H2O2"),
    @XmlEnumValue("КГ K2O")         KG_OXIDES_POTASSIUM("КГ K2O"),
    @XmlEnumValue("КГ KOH")         KG_HYDROXIDE_POTASSIUM("КГ KOH"),
    @XmlEnumValue("КГ N")           KG_NITROGEN("КГ N"),
    @XmlEnumValue("КГ NAOH")        KG_HYDROXIDE_SODIUM("КГ NAOH"),
    @XmlEnumValue("КГ P2O5")        KG_OXIDE_PHOSPHOROUS("КГ P2O5"),
    @XmlEnumValue("КГ U")           KG_URANIUM("КГ U"),
    @XmlEnumValue("КИ")             CURIE("КИ"),
    @XmlEnumValue("Л")              LITRE("Л"),
    @XmlEnumValue("Л 100% СПИРТА")  LITRE_ALCOHOL("Л 100% СПИРТА"),
    @XmlEnumValue("М")              METER("М"),
    @XmlEnumValue("М2")             METER_SQ("М2"),
    @XmlEnumValue("М3")             METER_CUBIC("М3"),
    @XmlEnumValue("ПАР")            PAIR("ПАР"),
    @XmlEnumValue("Т ГРП")          TON_CAPACITY("Т ГРП"),
    @XmlEnumValue("ШТ")             PIECES("ШТ");
    
    private final String value;
 
    Units(String v) {
        value = v;
    }
 
    @JsonValue
    public String value() {
        return value;
    }
 
    public static Units fromValue(String v) {
        for (Units c: Units.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
 
}