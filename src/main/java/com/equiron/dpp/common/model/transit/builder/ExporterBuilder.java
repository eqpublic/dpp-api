package com.equiron.dpp.common.model.transit.builder;

import com.equiron.dpp.common.model.transit.Exporter;
import com.equiron.dpp.common.model.transit.Taxpayer;
import com.github.javafaker.Faker;

/**
 * Билдер объектов Exporter
 */
public class ExporterBuilder implements HasTaxpayerBuilder {
    private Exporter exporter = new Exporter();
    private TransitDocBuilder transitDocBuilder;

    public ExporterBuilder(TransitDocBuilder transitDocBuilder) {
        this.transitDocBuilder = transitDocBuilder;
    }

    public ExporterBuilder withCountry(String country) {
        exporter.setCountry(country);
        return this;
    }

    @Override
    public void setTaxpayer(Taxpayer taxpayer) {
        exporter.setTaxpayer(taxpayer);
    }

    public TaxpayerBuilder<ExporterBuilder> createTaxpayer() {
        return new TaxpayerBuilder<>(this);
    }

    /**
     * Возвращает объект {@link TransitDocBuilder}
     */
    public TransitDocBuilder endExporter() {
        transitDocBuilder.setExporter(exporter);
        return transitDocBuilder;
    }

    public ExporterBuilder generate() {
        exporter.setCountry(Faker.instance().gameOfThrones().city());

        return this;
    }
}
