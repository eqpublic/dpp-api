package com.equiron.dpp.common.model.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.OrderedConfirmDocument;
import com.equiron.dpp.common.model.ws.OrderedDocument;
import com.equiron.dpp.common.model.ws.TransitDocument;

@Documentation("Запрос для передачи документов")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentsRequest")
public class DocumentsRequest {
    @Documentation("Токен НСПТ")
    @XmlElement(required = true)
    private String authToken;

    @XmlElements({
            @XmlElement(name = "confirmDocument", required = false, type = OrderedConfirmDocument.class, namespace = "urn:dpp"),
            @XmlElement(name = "usersDocument", required = false, type = UserFromMotp.class, namespace = "urn:dpp"),
            @XmlElement(name = "transitDocument", required = false, type = TransitDocument.class, namespace = "urn:dpp") })
    @XmlElementWrapper(required = true)
    private List<OrderedDocument> documents;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public List<OrderedDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<OrderedDocument> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "DocumentsRequest [authToken=*********, documents=" + documents + "]";
    }
    
    
}
