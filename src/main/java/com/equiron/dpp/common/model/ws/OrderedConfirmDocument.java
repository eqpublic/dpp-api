package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderedConfirmDocument")
public class OrderedConfirmDocument extends ConfirmDocument implements OrderedDocument {
    @Documentation("Порядковый номер документа в запросе или ответе. Используется, до присвоения UUID документу")
    @XmlElement(required = false)
    private Integer docNum;

    @Override
    public Integer getDocNum() {
        return docNum;
    }

    public void setDocNum(Integer docNum) {
        this.docNum = docNum;
    }

    @Override
    public String toString() {
        return "OrderedConfirmDocument [docNum=" + docNum + "]";
    }
    
}
