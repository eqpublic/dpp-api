package com.equiron.dpp.common.model;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@Documentation("Вид атрибута из списка")
@XmlEnum
public enum ClassifierStatusType {
    @XmlEnumValue("Черновик")               DRAFT("Черновик"),
    @XmlEnumValue("Опубликован")            PUBLISH("Опубликован"),
    @XmlEnumValue("Не опубликован")         NOT_PUBLISH("Не опубликован"),
    @XmlEnumValue("Опубликован частично")   PART_PUBLISH("Опубликован частично"); 
    
    private final String value;
 
    ClassifierStatusType(String v) {
        value = v;
    }
 
    public String value() {
        return value;
    }
 
    public static ClassifierStatusType fromValue(String v) {
        for (ClassifierStatusType c: ClassifierStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
 
}
