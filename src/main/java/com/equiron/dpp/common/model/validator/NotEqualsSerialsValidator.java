package com.equiron.dpp.common.model.validator;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.equiron.dpp.common.model.transit.HasSerial;
import com.equiron.dpp.common.model.transit.Product;

public class NotEqualsSerialsValidator implements ConstraintValidator<NotEqualsSerials, List<Product>>{
    @Autowired
    MessageSource messageSource;
    
    @Override
    public boolean isValid(List<Product> value, ConstraintValidatorContext context) {
        Set<String> foundStrings = ConcurrentHashMap.newKeySet();
        Set<String> duplicates = ConcurrentHashMap.newKeySet();
        
        context.disableDefaultConstraintViolation();
        
        for (Product val : value) {
            String ntin = val.getNtin();
            
            for (HasSerial serial : val.getItems()) {
                String uniqeKey = ntin + serial.getSerial();
            
                if (foundStrings.contains(uniqeKey)) {
                     duplicates.add(uniqeKey);
                     context.buildConstraintViolationWithTemplate(messageSource.getMessage("validator.sameSerialsNtin", Arrays.asList(ntin, serial.getSerial()).toArray(), new Locale("ru","RU")))
                     .addPropertyNode("serial")
                     .addConstraintViolation();
                 }
                 else {
                     foundStrings.add(uniqeKey);
                 }
            }
            
        }
        
        if (duplicates.size() > 0) return false;
        
        return true;
    }

}
