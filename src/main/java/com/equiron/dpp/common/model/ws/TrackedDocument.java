package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrackedDocument")
public class TrackedDocument extends SignedDocument {
    @Documentation("Уникальный номер документа в пределах ЕАЭС")
    @XmlElement(required = false)
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "TrackedDocument [uuid=" + uuid + "]";
    }
    
}
