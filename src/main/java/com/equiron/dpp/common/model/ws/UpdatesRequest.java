package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Запрос на получение новых сведений")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdatesRequest")
public class UpdatesRequest {
    @Documentation("Токен НСПТ")
    @XmlElement(required = true)
    private String authToken;

    @Documentation("Число полученное на предыдущем запросе, для получения только новых сообщений")
    @XmlElement(required = true)
    private Long since;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Long getSince() {
        return since;
    }

    public void setSince(Long since) {
        this.since = since;
    }

    @Override
    public String toString() {
        return "UpdatesRequest [authToken=" + authToken + ", since=" + since + "]";
    }
}
