package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Документ для передачи сведений об участнике товарооборота")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantDocument")
@XmlRootElement(namespace = "urn:dpp")
public class ParticipantDocument extends SignedDocument implements OrderedDocument {
    @Documentation("Порядковый номер документа в запросе или ответе")
    @XmlElement(required = true)
    private Integer docNum;
    
    @Override
    public Integer getDocNum() {
        return docNum;
    }
    
    public void setDocNum(Integer docNum) {
        this.docNum = docNum;
    }

}