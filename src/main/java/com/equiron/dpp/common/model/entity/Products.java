package com.equiron.dpp.common.model.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ClassifierStatusType;
import com.equiron.dpp.common.model.OperationType;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Товар")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product")
@XmlRootElement(namespace = "urn:dpp")
@Entity
@Table(name = "product")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class Products {
    @Id
    @XmlTransient
    private String uuid = UUID.randomUUID().toString();

    @Documentation("Уникальный номер записи в реестре товаров")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String id = UUID.randomUUID().toString();
    
    @Documentation("Id блока из классификатора")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String blockId;

    @Documentation("Версия блока")
    @XmlElement(required = true)
    @MinOccurs(1)
    private Long blockVersion;

    @Documentation("Артикул товара")
    @MinOccurs(1)
    @Facets(pattern = "\\d{14}")
    @XmlElement(required = true)
    private String ntin;

    @Documentation("Классифицирующая позиция")
    @Facets(pattern = "\\d{6,30}", maxLength = 30)
    @MinOccurs(1)
    @XmlElement(required = true)
    private String classPosition;

    @Documentation("Владелец товара, uid участника товарооброта")
    @Facets(maxLength = 24)
    @XmlElement(required = false)
    @MinOccurs(0)
    private String participantUid;

    @Documentation("Заполненные значения атрибутов блока в json формате")
    @XmlElement(required = true)
    @MinOccurs(1)
    private String listAttributes;

    @Documentation("Время добавления или последнее изменение продукта в реестре")
    @XmlElement(required = true)
    @MinOccurs(1)
    private long unixtime = System.currentTimeMillis();

    @Documentation("Наименование товара")
    @XmlElement(required = true)
    @Column(updatable = true, insertable = true)
    @MinOccurs(1)
    private String productName;

    @Documentation("Страна внесшая товар в реестр")
    @Column(updatable = true, insertable = true)
    @XmlTransient
    private String country;

    @XmlTransient
    @Column(updatable = true, insertable = true)
    private Long dateCreated;

    @XmlTransient
    @Column(updatable = true, insertable = true)
    private Long dateAdded;

    @Documentation("Версия классификатора товаров ЕАЭС")
    @XmlElement(required = true)
    @MinOccurs(1)
    private Long fromClassifier;

    @Documentation("Версия товара")
    @XmlElement(required = true)
    @MinOccurs(1)
    private Long version;

    @XmlTransient
    @Enumerated(EnumType.STRING)
    private ClassifierStatusType status;

    @Documentation("Операция по товару")
    @XmlElement(required = true)
    @Enumerated(EnumType.STRING)
    private OperationType operation;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Products() {
        // empty
    }

    public Products(Products pro) {
       this.blockId = pro.getBlockId();
       this.blockVersion = pro.getBlockVersion();
       this.classPosition = pro.getClassPosition();
       this.country = pro.getCountry();
       this.dateAdded = pro.getDateAdded();
       this.dateCreated = pro.getDateCreated();
       this.fromClassifier = pro.getFromClassifier();
       this.id = pro.getId();
       this.listAttributes = pro.getListAttributes();
       this.ntin = pro.getNtin();
       this.operation = pro.getOperation();
       this.participantUid = pro.getParticipantUid();
       this.productName = pro.getProductName();
       this.status = pro.getStatus();
       this.unixtime = pro.getUnixtime();
       this.uuid = UUID.randomUUID().toString();
       this.version = pro.getVersion();
    }

    public OperationType getOperation() {
        return operation;
    }

    public void setOperation(OperationType operation) {
        this.operation = operation;
    }

    public Long getFromClassifier() {
        return fromClassifier;
    }

    public void setFromClassifier(Long fromClassifier) {
        this.fromClassifier = fromClassifier;
    }

    public ClassifierStatusType getStatus() {
        return status;
    }

    public void setStatus(ClassifierStatusType status) {
        this.status = status;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNtin() {
        return ntin;
    }

    public void setNtin(String ntin) {
        this.ntin = ntin;
    }

    public String getClassPosition() {
        return classPosition;
    }

    public void setClassPosition(String classPosition) {
        this.classPosition = classPosition;
    }

    public String getListAttributes() {
        return listAttributes;
    }

    public void setListAttributes(String listAttributes) {
        this.listAttributes = listAttributes;
    }

    public long getUnixtime() {
        return unixtime;
    }

    public void setUnixtime(long unixtime) {
        this.unixtime = unixtime;
    }

    public String getParticipantUid() {
        return participantUid;
    }

    public void setParticipantUid(String participantUid) {
        this.participantUid = participantUid;
    }

    public Long getBlockVersion() {
        return blockVersion;
    }

    public void setBlockVersion(Long blockVersion) {
        this.blockVersion = blockVersion;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((classPosition == null) ? 0 : classPosition.hashCode());
        result = prime * result + ((listAttributes == null) ? 0 : listAttributes.hashCode());
        result = prime * result + ((ntin == null) ? 0 : ntin.hashCode());
        result = prime * result + ((participantUid == null) ? 0 : participantUid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Products other = (Products) obj;
        if (classPosition == null) {
            if (other.classPosition != null)
                return false;
        } else if (!classPosition.equals(other.classPosition))
            return false;
        if (listAttributes == null) {
            if (other.listAttributes != null)
                return false;
        } else if (!listAttributes.equals(other.listAttributes))
            return false;
        if (ntin == null) {
            if (other.ntin != null)
                return false;
        } else if (!ntin.equals(other.ntin))
            return false;
        if (participantUid == null) {
            if (other.participantUid != null)
                return false;
        } else if (!participantUid.equals(other.participantUid))
            return false;
        return true;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    @Override
    public String toString() {
        return "Products [id=" + id + ", uuid=" + uuid + ", blockId=" + blockId + ", blockVersion=" + blockVersion + ", ntin=" + ntin + ", classPosition="
                + classPosition + ", participantUid=" + participantUid + ", listAttributes=" + listAttributes + ", unixtime=" + unixtime + ", productName="
                + productName + ", country=" + country + ", dateCreated=" + dateCreated + ", dateAdded=" + dateAdded + ", fromClassifier=" + fromClassifier
                + ", version=" + version + ", status=" + status + ", operation=" + operation + "]";
    }

   
}
