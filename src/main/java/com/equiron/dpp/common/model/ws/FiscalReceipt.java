package com.equiron.dpp.common.model.ws;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Фискальныйц чек")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FiscalReceipt")
@XmlRootElement(name = "FiscalReceipt")
public class FiscalReceipt {
    @Documentation("Номер фискального накопителя")
    @XmlElement(required = false)
    private String fiscalDriveNumber;

    @Documentation("Номер ККТ")
    @XmlElement(required = true)
    @NotEmpty
    private String kktRegId;

    @Documentation("ИНН продавца")
    @XmlElement(required = true)
    @NotEmpty
    private String userInn;

    @Documentation("Номер чека")
    @XmlElement(required = true)
    private long fiscalDocumentNumber;

    @Documentation("Время создания чека")
    @XmlElement(required = true)
    private long dateTime;

    @Documentation("фискальная подпись")
    @XmlElement(required = false)
    @MinOccurs(0)
    private long fiscalSign;

    @Documentation("Тип операции")
    @XmlElement(required = true)
    private int operationType;

    @Documentation("Сумма по чеку")
    @XmlElement(required = false)
    private BigDecimal totalSum;

    @Documentation("Сумма наличными")
    @XmlElement(required = false)
    private BigDecimal cashTotalSum;

    @Documentation("Сумма по карте")
    @XmlElement(required = false)
    private BigDecimal ecashTotalSum;

    @Documentation("Сумма предоплаты")
    @XmlElement(required = false)
    private BigDecimal prepaidSum;

    @Documentation("Сумма кредита")
    @XmlElement(required = false)
    private BigDecimal creditSum;

    @Documentation("Сумма резерва")
    @XmlElement(required = false)
    private BigDecimal provisionSum;

    @Documentation("Тип налогооблажения")
    @XmlElement(required = false)
    @MinOccurs(0)
    private long taxationType;

    @Documentation("Налогооблагаемая сумма")
    @XmlElement(required = false)
    private BigDecimal ndsNo;

    @Documentation("Товары")
    @XmlElementWrapper(required = true)
    @XmlElement(name = "FiscalItem", required = true, namespace = "urn:dpp")
    @Valid
    private List<FiscalItem> fiscalItems;

    @Documentation("Электронные данные чека")
    @XmlElement(required = false)
    private String rawData;

    @Documentation("Подпись электронная по чеку")
    @XmlElement(required = false)
    private String messageFiscalSign;

    public FiscalReceipt() {
        // empty
    }

    public String getFiscalDriveNumber() {
        return fiscalDriveNumber;
    }

    public void setFiscalDriveNumber(String fiscalDriveNumber) {
        this.fiscalDriveNumber = fiscalDriveNumber;
    }

    public String getKktRegId() {
        return kktRegId;
    }

    public void setKktRegId(String kktRegId) {
        this.kktRegId = kktRegId;
    }

    public String getUserInn() {
        return userInn;
    }

    public void setUserInn(String userInn) {
        this.userInn = userInn;
    }

    public long getFiscalDocumentNumber() {
        return fiscalDocumentNumber;
    }

    public void setFiscalDocumentNumber(long fiscalDocumentNumber) {
        this.fiscalDocumentNumber = fiscalDocumentNumber;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public long getFiscalSign() {
        return fiscalSign;
    }

    public void setFiscalSign(long fiscalSign) {
        this.fiscalSign = fiscalSign;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public BigDecimal getCashTotalSum() {
        return cashTotalSum;
    }

    public void setCashTotalSum(BigDecimal cashTotalSum) {
        this.cashTotalSum = cashTotalSum;
    }

    public BigDecimal getEcashTotalSum() {
        return ecashTotalSum;
    }

    public void setEcashTotalSum(BigDecimal ecashTotalSum) {
        this.ecashTotalSum = ecashTotalSum;
    }

    public BigDecimal getPrepaidSum() {
        return prepaidSum;
    }

    public void setPrepaidSum(BigDecimal prepaidSum) {
        this.prepaidSum = prepaidSum;
    }

    public BigDecimal getCreditSum() {
        return creditSum;
    }

    public void setCreditSum(BigDecimal creditSum) {
        this.creditSum = creditSum;
    }

    public BigDecimal getProvisionSum() {
        return provisionSum;
    }

    public void setProvisionSum(BigDecimal provisionSum) {
        this.provisionSum = provisionSum;
    }

    public long getTaxationType() {
        return taxationType;
    }

    public void setTaxationType(long taxationType) {
        this.taxationType = taxationType;
    }

    public BigDecimal getNdsNo() {
        return ndsNo;
    }

    public void setNdsNo(BigDecimal ndsNo) {
        this.ndsNo = ndsNo;
    }

    public List<FiscalItem> getFiscalItems() {
        return fiscalItems;
    }

    public void setFiscalItems(List<FiscalItem> fiscalItems) {
        this.fiscalItems = fiscalItems;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getMessageFiscalSign() {
        return messageFiscalSign;
    }

    public void setMessageFiscalSign(String messageFiscalSign) {
        this.messageFiscalSign = messageFiscalSign;
    }

    @Override
    public String toString() {
        return "FiscalReceipt [fiscalDriveNumber=" + fiscalDriveNumber + ", kktRegId=" + kktRegId + ", userInn="
                + userInn + ", fiscalDocumentNumber=" + fiscalDocumentNumber + ", dateTime=" + dateTime
                + ", fiscalSign=" + fiscalSign + ", operationType=" + operationType + ", totalSum=" + totalSum
                + ", cashTotalSum=" + cashTotalSum + ", ecashTotalSum=" + ecashTotalSum + ", prepaidSum=" + prepaidSum
                + ", creditSum=" + creditSum + ", provisionSum=" + provisionSum + ", taxationType=" + taxationType
                + ", ndsNo=" + ndsNo + ", fiscalItems=" + fiscalItems + ", rawData=" + rawData + ", messageFiscalSign="
                + messageFiscalSign + "]";
    }

}
