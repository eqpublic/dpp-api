package com.equiron.dpp.common.model.transit;

import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Taxpayer")
public class Taxpayer {
    @Documentation("Трехзначный цифровой код страны из справочника")
    @Facets(pattern = "(051)|(112)|(398)|(417)|(643)")
    @XmlElement(required = true)
    @NotBlank
    private String country;
    
    @Documentation("Наименование или фамилия, имя, отчество")
    @Facets(minLength=3, maxLength=256)
    @XmlElement(required = true)
    @NotBlank
    private String name;
    
    @Documentation("Идентификатор налогоплательщика")
    @XmlElement(required = true)
    @Facets(pattern = "[\\d]{9,24}")
    @NotBlank
    private String id;

    public Taxpayer() {
    }

    public Taxpayer(String country, String name, String id) {
        this.country = country;
        this.name = name;
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Taxpayer [country=" + country + ", name=" + name + ", id=" + id + "]";
    }
    
}
