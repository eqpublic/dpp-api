package com.equiron.dpp.common.model.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.ConfirmDocument;
import com.equiron.dpp.common.model.ws.TrackedDocument;

@Documentation("Получаемые сведения")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdatesResponse")
public class UpdatesResponse {
    @XmlElements({
        @XmlElement(name = "confirmDocument", type = ConfirmDocument.class, namespace = "urn:dpp"),
        @XmlElement(name = "fiscalDocument", type = FiscalDocument.class, namespace = "urn:dpp"),
        @XmlElement(name = "document", type = TrackedDocument.class, namespace = "urn:dpp") })
    @XmlElementWrapper(required = true)
    private List<TrackedDocument> documents;

    @Documentation("Число полученное на предыдущем запросе, для получения только новых сообщений")
    @XmlElement(required = true)
    private long since;
    
    public List<TrackedDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<TrackedDocument> trackedDocumentList) {
        this.documents = trackedDocumentList;
    }

    public long getSince() {
        return since;
    }

    public void setSince(long l) {
        this.since = l;
    }

    @Override
    public String toString() {
        return "UpdatesResponse [documents=" + documents + ", since=" + since + "]";
    }
    
}
