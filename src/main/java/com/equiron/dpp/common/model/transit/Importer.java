package com.equiron.dpp.common.model.transit;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Importer")
public class Importer {
    @Documentation("Трехзначный цифровой код страны из справочника")
    @Facets(pattern = "(051)|(112)|(398)|(417)|(643)")
    @XmlElement(required = true)
    @NotBlank
    private String country;
    
    @Documentation("Наименование импортера")
    @XmlElement(required = true)
    @Valid
    @NotNull
    private Taxpayer taxpayer;

    public Importer() {
    }

    public Importer(String country, Taxpayer taxpayer) {
        this.country = country;
        this.taxpayer = taxpayer;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Taxpayer getTaxpayer() {
        return taxpayer;
    }

    public void setTaxpayer(Taxpayer taxpayer) {
        this.taxpayer = taxpayer;
    }

    @Override
    public String toString() {
        return "Importer [country=" + country + ", taxpayer=" + taxpayer + "]";
    }
}
