package com.equiron.dpp.common.model.transit.builder;

import com.equiron.dpp.common.model.transit.Importer;
import com.equiron.dpp.common.model.transit.Taxpayer;
import com.github.javafaker.Faker;

/**
 * Билдер для {@link Importer}
 */
public class ImporterBuilder implements HasTaxpayerBuilder {
    private Importer importer = new Importer();
    private TransitDocBuilder transitDocBuilder;

    public ImporterBuilder(TransitDocBuilder transitDocBuilder) {
        this.transitDocBuilder = transitDocBuilder;
    }

    public ImporterBuilder withCountry(String country) {
        importer.setCountry(country);
        return this;
    }

    @Override
    public void setTaxpayer(Taxpayer taxpayer) {
        importer.setTaxpayer(taxpayer);
    }

    public TaxpayerBuilder<ImporterBuilder> createTaxpayer() {
        return new TaxpayerBuilder<>(this);
    }

    /**
     * Возвращает объект {@link TransitDocBuilder}
     */
    public TransitDocBuilder endImporter() {
        transitDocBuilder.setImporter(importer);
        return transitDocBuilder;
    }

    public ImporterBuilder generate() {
        importer.setCountry(Faker.instance().gameOfThrones().city());

        return this;
    }
}
