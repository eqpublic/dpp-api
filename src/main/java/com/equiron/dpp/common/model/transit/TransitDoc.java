package com.equiron.dpp.common.model.transit;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.transit.builder.TransitDocBuilder;
import com.equiron.dpp.common.model.validator.NotEqualsSerials;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransitDoc")
@XmlRootElement(namespace = "urn:dpp")
public class TransitDoc {
    @Documentation("Уникальный номер документа в пределах ЕАЭС")
    @XmlElement(required = true)
    @NotBlank
    @Facets(pattern = "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}")
    private String uuid;
    
    @Documentation("Данные об экспортере")
    @XmlElement(required = true)
    @Valid
    @NotNull
    private Exporter exporter;
    
    @Documentation("Данные об импортере")
    @XmlElement(required = true)
    @Valid
    @NotNull
    private Importer importer;
    
    @Documentation("Регистрационный номер сопроводительного документа")
    @XmlElement(required = true)
    @NotBlank
    @Facets(minLength =1 , maxLength = 64)
    private String accompDocId;
    
    @Documentation("Дата выписки сопроводительного документа")
    @XmlElement(required = true)
    @NotBlank
    private String accompDocDate;
    
    @Documentation("Сведения о пунктах назначения (доставки, разгрузки) товара, подлежащего прослеживаемости, в соответствии с сопроводительными документами")
    @XmlElementWrapper(required = true)
    @XmlElement(name = "destination", required = false)
    private List<Destination> destinations;
    
    @Documentation("Товар")
    @XmlElementWrapper(required = true)
    @XmlElement(name = "product", required = true)
    @NotNull
    @NotEqualsSerials
    private List<@NotNull Product> products;

    public TransitDoc() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Exporter getExporter() {
        return exporter;
    }

    public void setExporter(Exporter exporter) {
        this.exporter = exporter;
    }

    public Importer getImporter() {
        return importer;
    }

    public void setImporter(Importer importer) {
        this.importer = importer;
    }

    public String getAccompDocId() {
        return accompDocId;
    }

    public void setAccompDocId(String accompDocId) {
        this.accompDocId = accompDocId;
    }

    public String getAccompDocDate() {
        return accompDocDate;
    }

    public void setAccompDocDate(String accompDocDate) {
        this.accompDocDate = accompDocDate;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Возвращает билдер для {@link TransitDoc}
     */
    public static TransitDocBuilder create() {
        return new TransitDocBuilder();
    }

    @Override
    public String toString() {
        return "TransitDoc [uuid=" + uuid + ", exporter=" + exporter + ", importer=" + importer + ", accompDocId=" + accompDocId + ", accompDocDate="
                + accompDocDate + ", destinations=" + destinations + ", products=" + products + "]";
    }
    
}
