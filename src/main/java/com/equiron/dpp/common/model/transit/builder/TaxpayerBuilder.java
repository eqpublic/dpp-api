package com.equiron.dpp.common.model.transit.builder;

import com.equiron.dpp.common.model.transit.Taxpayer;
import com.github.javafaker.Faker;

import java.util.UUID;

/**
 * Билдер объектов {@link Taxpayer}
 */
public class TaxpayerBuilder<T extends HasTaxpayerBuilder> {
    private Taxpayer taxpayer = new Taxpayer();

    private T exporterBuilder;

    public TaxpayerBuilder(T exporterBuilder) {
        this.exporterBuilder = exporterBuilder;
    }

    public TaxpayerBuilder<T> withCountry(String country) {
        taxpayer.setCountry(country);
        return this;
    }

    public TaxpayerBuilder<T> withName(String name) {
        taxpayer.setName(name);
        return this;
    }

    public TaxpayerBuilder<T> withId(String id) {
        taxpayer.setId(id);
        return this;
    }

    public T endTaxpayer() {
        exporterBuilder.setTaxpayer(taxpayer);
        return exporterBuilder;
    }

    public TaxpayerBuilder<T> generate() {
        Faker faker = new Faker();
        taxpayer.setId(UUID.randomUUID().toString());
        taxpayer.setName(faker.name().name());
        taxpayer.setCountry(faker.gameOfThrones().city());

        return this;
    }
}
