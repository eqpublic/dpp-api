package com.equiron.dpp.common.model.ws;

public interface OrderedDocument {
    Integer getDocNum();
}
