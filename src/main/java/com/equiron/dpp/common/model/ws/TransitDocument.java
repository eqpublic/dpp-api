package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransitDocument")
@JsonTypeInfo(use = Id.CLASS)
public class TransitDocument extends SignedDocument implements OrderedDocument {
    @Documentation("Порядковый номер документа в запросе или ответе. Используется до присвоения UUID документу")
    @XmlElement(required = false)
    private Integer docNum;

    @Documentation("Уникальный номер документа в пределах ЕАЭС")
    @XmlElement(required = false)
    private String uuid;

    @Override
    public Integer getDocNum() {
        return docNum;
    }
    
    public void setDocNum(Integer docNum) {
        this.docNum = docNum;
    }
    
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "TransitDocument [docNum=" + docNum + ", uuid=" + uuid + "]";
    }
    
}
