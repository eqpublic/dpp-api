package com.equiron.dpp.common.model.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ConfirmationType;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Подтверждающий документ")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmDocument")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class ConfirmDocument extends TrackedDocument {
    @Documentation("Статус сообщения")
    @XmlElement(name = "status", required = true)
    private ConfirmationType status;

    @Documentation("Список ошибок в случае отклонения, необязательный параметр")
    @XmlElementWrapper
    @XmlElement(name = "error", required = true)
    private List<Error> errors;

    public ConfirmationType getStatus() {
        return status;
    }

    public void setStatus(ConfirmationType status) {
        this.status = status;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ConfirmDocument [status=" + status + ", errors=" + errors + "]";
    }
}
