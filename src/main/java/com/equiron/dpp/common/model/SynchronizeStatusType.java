package com.equiron.dpp.common.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "")
public enum SynchronizeStatusType {
    @XmlEnumValue("Создан")             CREATED("Создан"),
    @XmlEnumValue("Отправлен")          SEND("Отправлен"),
    @XmlEnumValue("Подтвержден")        CONFIRM("Подтвержден");
    
    private final String value;
 
    SynchronizeStatusType(String v) {
        value = v;
    }
 
    public String value() {
        return value;
    }
 
    public static SynchronizeStatusType fromValue(String v) {
        for (SynchronizeStatusType c: SynchronizeStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
