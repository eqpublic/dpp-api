package com.equiron.dpp.common.model.transit;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "")
public enum TrackingType {
    @XmlEnumValue("физическая")             PHISICAL("физическая"),
    @XmlEnumValue("документальная")         DOCUMENTAL("документальная");
    
    private final String value;
 
    TrackingType(String v) {
        value = v;
    }
 
    public String value() {
        return value;
    }
 
    public static TrackingType fromValue(String v) {
        for (TrackingType c: TrackingType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
 
}
