package com.equiron.dpp.common.model;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeliveredType")
@Documentation("Статус документа")
public enum DeliveredType {
    @Documentation("Документ получен ЦПТ")
    DELIVERED,
    
    @Documentation("Ошибка валидации документа")
    VALIDATION_ERROR
}
