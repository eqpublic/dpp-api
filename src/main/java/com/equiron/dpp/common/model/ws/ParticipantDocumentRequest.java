package com.equiron.dpp.common.model.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@Documentation("Запрос для передачи реестра участников")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantDocumentRequest")
public class ParticipantDocumentRequest {
    @Documentation("Токен НСПТ")
    @XmlElement(required = true)
    private String authToken;

    @Documentation("Список сведений")
    @XmlElement(name = "participantDocument", required = true, type = ParticipantDocument.class, namespace = "urn:dpp")
    @XmlElementWrapper(required = true)
    private List<OrderedDocument> documents;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public List<OrderedDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<OrderedDocument> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "ParticipantDocumentRequest [authToken=" + authToken + ", documents=" + documents + "]";
    }
}
