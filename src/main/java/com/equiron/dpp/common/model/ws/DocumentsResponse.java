package com.equiron.dpp.common.model.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.OrderedConfirmDocument;

@Documentation("Получаемые сведения")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentsResponse")
public class DocumentsResponse {
    @Documentation("Список подтверждающих документов")
    @XmlElement(required = true, name = "confirmDocument")
    @XmlElementWrapper(required = true)
    private List<OrderedConfirmDocument> confirmDocuments;

    public List<OrderedConfirmDocument> getConfirmDocuments() {
        return confirmDocuments;
    }

    public void setConfirmDocuments(List<OrderedConfirmDocument> confirmDocuments) {
        this.confirmDocuments = confirmDocuments;
    }
}
