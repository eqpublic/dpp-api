package com.equiron.dpp.common.model.transit.builder;

import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.IntStream;

import com.equiron.dpp.common.model.transit.Destination;
import com.equiron.dpp.common.model.transit.Exporter;
import com.equiron.dpp.common.model.transit.Importer;
import com.equiron.dpp.common.model.transit.Product;
import com.equiron.dpp.common.model.transit.TransitDoc;
import com.github.javafaker.Faker;

/**
 * Класс - билдер для {@link TransitDoc}
 */
public class TransitDocBuilder {
    private TransitDoc doc = new TransitDoc();

    public TransitDocBuilder withUuid(String uuid) {
        doc.setUuid(uuid);
        return this;
    }

    public ExporterBuilder createExporter() {
        return new ExporterBuilder(this);
    }

    public ImporterBuilder createImporter() {
        return new ImporterBuilder(this);
    }

    public TransitDocBuilder withAccompDocId(String accompDocId) {
        doc.setAccompDocId(accompDocId);
        return this;
    }

    public TransitDocBuilder withAccompDocDate(String accompDocDate) {
        doc.setAccompDocDate(accompDocDate);
        return this;
    }

    public TransitDocBuilder addDestination(Destination destination) {
        if (null == doc.getDestinations()) {
            doc.setDestinations(new ArrayList<>());
        }

        doc.getDestinations().add(destination);

        return this;
    }

    public void addProduct(Product product) {
        if (null == doc.getProducts()) {
            doc.setProducts(new ArrayList<>());
        }

        doc.getProducts().add(product);
    }

    public ProductBuilder createProduct() {
        return new ProductBuilder(this);
    }

    /**
     * Возвращает объект {@link TransitDoc}, инициализированный билдером
     */
    public TransitDoc end() {
        return doc;
    }

    public void setExporter(Exporter exporter) {
        doc.setExporter(exporter);
    }

    public void setImporter(Importer importer) {
        doc.setImporter(importer);
    }

    public TransitDocBuilder generate() {
        Faker faker = new Faker();

        doc.setUuid(UUID.randomUUID().toString());
        doc.setAccompDocDate(faker.date().toString());
        doc.setAccompDocId(faker.number().digits(10));

        return this;
    }

    public TransitDocBuilder generateDestination() {
        addDestination(new Destination(UUID.randomUUID().toString(), Faker.instance().address().fullAddress()));
        return this;
    }

    public TransitDocBuilder generateDestination(int count) {
        IntStream.range(0, count).forEach(c -> addDestination(new Destination(UUID.randomUUID().toString(), Faker.instance().address().fullAddress())));

        return this;
    }
}
