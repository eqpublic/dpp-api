package com.equiron.dpp.common.model.transit.builder;

import java.util.ArrayList;
import java.util.stream.IntStream;

import com.equiron.dpp.common.model.transit.HasSerial;
import com.equiron.dpp.common.model.transit.Product;
import com.equiron.dpp.common.model.transit.Units;
import com.github.javafaker.Faker;

public class ProductBuilder {
    private Product product = new Product();

    private TransitDocBuilder transitDocBuilder;

    public ProductBuilder(TransitDocBuilder transitDocBuilder) {

        this.transitDocBuilder = transitDocBuilder;
    }

    public ProductBuilder withNtin(String ntin) {
        product.setNtin(ntin);
        return this;
    }

    public ProductBuilder withAccompDocProductNum(Integer accompDocProductNum) {
        product.setAccompDocProductNum(accompDocProductNum);
        return this;
    }

    public ProductBuilder withDeclarationNum(String declarationNum) {
        product.setDeclarationNum(declarationNum);
        return this;
    }

    public ProductBuilder withDeclarationProductNum(Integer declarationProductNum) {
        product.setDeclarationProductNum(declarationProductNum);
        return this;
    }

    public ProductBuilder withUnits(Units units) {
        product.setUnits(units);
        return this;
    }

    public ProductBuilder withQuantity(Integer quantity) {
        product.setQuantity(quantity);
        return this;
    }

    public ProductBuilder withTrackingType(String trackingType) {
        product.setTrackingType(trackingType);
        return this;
    }

    public ProductBuilder addSerial(HasSerial hasSerial) {
        if (null == product.getItems()) {
            product.setItems(new ArrayList<>());
        }

        product.getItems().add(hasSerial);

        return this;
    }

    /**
     * Возвращает объект {@link TransitDocBuilder}
     */
    public TransitDocBuilder endProduct() {
        transitDocBuilder.addProduct(product);
        return transitDocBuilder;
    }

    public ProductBuilder generate() {
        Faker faker = new Faker();
        product.setTrackingType("физическая");
        product.setQuantity(faker.random().nextInt(0, 100));
        product.setUnits(Units.GRAMM);
        product.setDeclarationProductNum(faker.random().nextInt(0, 100));
        product.setDeclarationNum(faker.lorem().fixedString(5));
        product.setAccompDocProductNum(faker.random().nextInt(0, 100));
        product.setNtin(faker.code().gtin13());

        return this;
    }

    public ProductBuilder generateSerial() {
        addSerial(new HasSerial(Faker.instance().code().asin()));

        return this;
    }

    public ProductBuilder generateSerial(int count) {
        IntStream.range(0, count).forEach(c -> addSerial(new HasSerial(Faker.instance().code().asin())));
        return this;
    }

  
}
