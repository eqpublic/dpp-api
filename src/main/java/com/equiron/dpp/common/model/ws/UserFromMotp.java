package com.equiron.dpp.common.model.ws;

import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MaxOccurs;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserFromMotp", namespace = "urn:dpp")
@Documentation("Пользователь от МОТП")
public class UserFromMotp implements OrderedDocument {
    @Documentation("ID пользователя в МОТП")
    @XmlElement(required = true)
    @NotBlank
    private String userId;

    @Documentation("ИИН")
    @XmlElement(required = true)
    @NotBlank
    @Facets(length = 12)
    private String iin;

    @Documentation("Адрес электронной почты")
    @MaxOccurs(1)
    @MinOccurs(1)
    @Facets(minLength = 6, maxLength = 255)
    @XmlElement(required = true)
    private String email;

    @Documentation("Признак подтверждения аккаунта")
    @XmlElement(required = true)
    private boolean confirmed;

    @Documentation("Идентификатор участника оборота")
    @MinOccurs(1)
    @Facets(pattern="[0-9]{1,24}",maxLength = 24)
    @XmlElement(required = true)
    private String participantId;

    @Documentation("Наименование участника оборота")
    @MinOccurs(1)
    @Facets(maxLength = 1024)
    @XmlElement(required = true)
    private String participantName;

    @Documentation("Трехзначный цифровой код страны из справочника, в котором участник оборота является резидентом")
    @MinOccurs(1)
    @Facets(pattern="[0-9]{3}",length = 3)
    @XmlElement(required = true)
    private String country;

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserFromMotp [userId=" + userId + ", iin=" + iin + ", email=" + email + ", confirmed=" + confirmed + ", participantId=" + participantId
                + ", participantName=" + participantName + ", country=" + country + "]";
    }

    @Override
    public Integer getDocNum() {
        return null;
    }
}
