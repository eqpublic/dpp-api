package com.equiron.dpp.common.model.entity;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.ConfirmDocument;

@Documentation("Запрос для подтверждения приема сведений")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmRequest")
public class ConfirmRequest {
    @Documentation("Токен НСПТ")
    @XmlElement(required = true)
    private String authToken;

    @Documentation("Подтверждение о приеме сведений")
    @XmlElement(name = "confirmation", required = false)
    private ConfirmDocument confirmation;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public ConfirmDocument getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(ConfirmDocument confirmation) {
        this.confirmation = confirmation;
    }

    @Override
    public String toString() {
        return "ConfirmRequest [authToken=" + authToken + ", confirmation=" + confirmation + "]";
    }
 
    
}
