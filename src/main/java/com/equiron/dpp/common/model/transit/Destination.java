package com.equiron.dpp.common.model.transit;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Destination")
public class Destination {
    @XmlElement(required = true)
    @Documentation("Идентификатор пункта назначения")
    @Facets(maxLength = 24)
    private String id;
    
    @Documentation("Адрес")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(maxLength = 1024)
    private String address;

    public Destination() {
    }

    public Destination(String id, String address) {
        this.id = id;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Destination [id=" + id + ", address=" + address + "]";
    }
    
}
