package com.equiron.dpp.common.model;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@Documentation("Вид атрибута из списка")
@XmlEnum
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationType")
//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public enum OperationType {
    ADD,
    DELETED,
    CHANGE;
    
}
