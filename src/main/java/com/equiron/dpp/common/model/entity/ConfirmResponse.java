package com.equiron.dpp.common.model.entity;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Содержимое ответа")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmResponse")
public class ConfirmResponse {
        @Documentation("Результат")
        @XmlElement(required = true, name = "result")
        private String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
}
