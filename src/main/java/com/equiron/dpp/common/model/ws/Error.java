package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Ошибка по документу")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Error")
public class Error {
    @Documentation("Код ошибки или краткое название")
    @XmlElement(required = true)
    private String field;
    
    @Documentation("Подробное описание ошибки")
    @XmlElement(required = true)
    private String text;

    public Error() {
        // empty
    }

    public Error(String field, String text) {
        this.field = field;
        if (text == null) {
            text = "text is null";
        }
        this.text = text;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Error [field=" + field + ", text=" + text + "]";
    }
    
}

