package com.equiron.dpp.common.model.entity;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Участник товарооборота")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Participant")
@XmlRootElement(namespace = "urn:dpp")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
@Entity
public class Participant {
    @Id
    @XmlTransient
    private String id = UUID.randomUUID().toString();

    @XmlTransient
    @MinOccurs(0)
    private long since;

    @Documentation("Версия изменений")
    @MinOccurs(1)
    private long version;

    @Documentation("Идентификатор участника оборота")
    @MinOccurs(1)
    @Facets(pattern="[0-9]{1,24}",maxLength = 24)
    private String uid;

    @Documentation("Наименование участника оборота")
    @MinOccurs(1)
    @Facets(maxLength = 1024)
    private String name;

    @Documentation("Трехзначный цифровой код страны из справочника, в котором участник оборота является резидентом")
    @MinOccurs(1)
    @Facets(pattern = "\\d{3}", maxLength = 3)
    private String country;
    
    @Documentation("Идентификационный номер налогоплательщика.Зависит от страны, в которой он зарегистрирован")
    @MinOccurs(0)
    @Facets(pattern="[0-9]{1,24}",maxLength = 24)
    private String taxpayerId;
    
    @Documentation("Дата регистрации участника оборота. YYYY-MM-DD по стандарту формата ISO 8601")
    @MinOccurs(1)
    @XmlSchemaType(name = "date")
    private String regDate;
    
    @Documentation("Дата снятия с учета в реестре участников оборота. YYYY-MM-DD по стандарту формата ISO 8601")
    @MinOccurs(0)
    @XmlElement(required = false)
    @XmlSchemaType(name = "date")
    private String unregDate;
    
    @Documentation("Расширенная информация об участнике, добавленная им добровольно")
    @XmlTransient
    private String extended;
    
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public long getSince() {
        return since;
    }

    public void setSince(long since) {
        this.since = since;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTaxpayerId() {
        return taxpayerId;
    }

    public void setTaxpayerId(String taxpayerId) {
        this.taxpayerId = taxpayerId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getUnregDate() {
        return unregDate;
    }

    public void setUnregDate(String unregDate) {
        this.unregDate = unregDate;
    }

    public void apply(ParticipantDto dto) {
//        this.country = dto.getCountry();
//        this.extended = dto.getExtended();
        this.uid = dto.getUid();
        this.name = dto.getName();
        this.regDate = dto.getRegDate();
//        this.since = dto.getSince();
        this.taxpayerId = dto.getTaxpayerId();
//        this.unregDate = dto.getUnregDate();
    }

    @Override
    public String toString() {
        return "Participant [id=" + id + ", since=" + since + ", version=" + version + ", uid=" + uid + ", name=" + name + ", country=" + country
                + ", taxpayerId=" + taxpayerId + ", regDate=" + regDate + ", unregDate=" + unregDate + ", extended=" + extended + "]";
    }


    
}
