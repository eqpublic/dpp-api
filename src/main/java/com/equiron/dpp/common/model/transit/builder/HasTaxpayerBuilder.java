package com.equiron.dpp.common.model.transit.builder;

import com.equiron.dpp.common.model.transit.Taxpayer;

public interface HasTaxpayerBuilder {
    void setTaxpayer(Taxpayer taxpayer);
}
