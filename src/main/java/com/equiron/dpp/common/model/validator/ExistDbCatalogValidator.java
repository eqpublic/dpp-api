package com.equiron.dpp.common.model.validator;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;

import com.equiron.dpp.common.model.entity.Products;

@Component
public class ExistDbCatalogValidator implements ConstraintValidator<ExistDbCatalog, String> {
	@PersistenceContext
	EntityManager em;
	
    @Override
    public boolean isValid(String newNtin, ConstraintValidatorContext constraintValidatorContext) {
    	List<Products> list = em.createQuery("SELECT p FROM Products p WHERE p.ntin = :ntin ORDER BY p.unixtime", Products.class)
		.setParameter("ntin", newNtin)
		.setMaxResults(100)
		.getResultList();
    	
    	return list.isEmpty() ? false : true;
    }
}