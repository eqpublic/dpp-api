package com.equiron.dpp.common.model.transit;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

@Documentation("Упаковка")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Box")
public class Box extends HasSerial {
    @XmlElementWrapper(required = true)
    @XmlElements({
        @XmlElement(name = "box",  required = true, type = Box.class),
        @XmlElement(name = "item", required = true, type = Item.class) })
    private List<HasSerial> items;

    public List<HasSerial> getItems() {
        return items;
    }

    public void setItems(List<HasSerial> items) {
        this.items = items;
    }
}
