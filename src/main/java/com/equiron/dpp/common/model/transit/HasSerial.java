package com.equiron.dpp.common.model.transit;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Documentation("Серийный номер упаковки или товара товара")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HasSerial")
public class HasSerial {
    @XmlElement(required = true)
    private String serial;

    public HasSerial() {
    }

    public HasSerial(String serial) {
        this.serial = serial;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
