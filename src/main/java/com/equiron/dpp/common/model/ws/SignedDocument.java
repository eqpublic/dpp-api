package com.equiron.dpp.common.model.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignedDocument")
public class SignedDocument {
    @Documentation("Формат документа. Пример X-1  XML документ первой версии")
    @XmlElement(required = false)
    private String format;
    
    @Documentation("Первичный документ передаваемый в виде текста")
    @XmlElement(required = true, defaultValue="")
    private String doc;
    
    @Documentation("Контрольная сумма документа")
    @XmlElement(required = false)
    private String signature;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "SignedDocument [format=" + format + ", doc=" + doc + ", signature=****]";
    }
    
}
