package com.equiron.dpp.api.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.Error;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Содержимое ответа на запрос получения подтверждения")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryConfirmationResponse")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class QueryConfirmationResponse {
    @Documentation("Код подтверждения")
    @XmlElement(required = true)
    @MinOccurs(1)
    private DocumentConfirmation documentConfirmation; 

    @Documentation("Список ошибок в случае отклонения, необязательный параметр")
    @XmlElementWrapper
    @XmlElement(name = "error", required = false)
    @MinOccurs(0)
    private List<Error> errors;
    
    public QueryConfirmationResponse() {
        // empty
    }
    
    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public DocumentConfirmation getDocumentConfirmation() {
        return documentConfirmation;
    }

    public void setDocumentConfirmation(DocumentConfirmation documentConfirmation) {
        this.documentConfirmation = documentConfirmation;
    }

    @Override
    public String toString() {
        return "QueryConfirmationResponse [documentConfirmation=" + documentConfirmation + ", errors=" + errors + "]";
    }


}
