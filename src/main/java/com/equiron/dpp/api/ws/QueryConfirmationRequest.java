package com.equiron.dpp.api.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Сведения о новых участниках")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryConfirmationRequest")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class QueryConfirmationRequest {
    @Documentation("Идентификатор электронного документа (сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docId;
    
    @Documentation("Код государства-члена  (НСПТ отправителя сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "\\d{3}", maxLength = 3)
    private String countryCode;

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "QueryConfirmationRequest [docId=" + docId + ", countryCode=" + countryCode + "]";
    }
    
}
