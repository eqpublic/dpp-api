package com.equiron.dpp.api.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.DeliveryConfirmationType;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Подтверждение о получении сведений")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvideConfirmationRequest")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class ProvideConfirmationRequest {
    @Documentation("Идентификатор электронного документа (сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docId;
    
    @Documentation("Идентификатор электронного документа (сведений), в ответ на который был сформирован данный электронный документ (сведения)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docRefId;

    @Documentation("Код государства-члена  (НСПТ отправителя сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "\\d{3}", maxLength = 3)
    private String countryCode;
    
    @Documentation("Статус сообщения")
    @XmlElement(name = "status", required = true)
    @MinOccurs(1)
    private DeliveryConfirmationType status;

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocRefId() {
        return docRefId;
    }

    public void setDocRefId(String docRefId) {
        this.docRefId = docRefId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public DeliveryConfirmationType getStatus() {
        return status;
    }

    public void setStatus(DeliveryConfirmationType status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ProvideConfirmationResponse [docId=" + docId + ", docRefId=" + docRefId + ", countryCode=" + countryCode + ", status=" + status + "]";
    }

    
    
}
