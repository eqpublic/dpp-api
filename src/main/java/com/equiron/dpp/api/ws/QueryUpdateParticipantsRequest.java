package com.equiron.dpp.api.ws;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Запрос на получение новых участников")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryParticipantsRequest")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class QueryUpdateParticipantsRequest {
    @Documentation("Идентификатор электронного документа (сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docId;
    
//    @Documentation("Идентификатор электронного документа (сведений), в ответ на который был сформирован данный электронный документ (сведения)")
//    @XmlElement(required = false)
//    @MinOccurs(0)
//    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
//    private String docRefId;

    @Documentation("Дата и время создания обновлений классификатора товаров ЕАЭС")
    @XmlElement(required = true)
    @MinOccurs(1)
    @XmlSchemaType(name = "dateTime")
    private String docDateTime;
    
    @Documentation("Код государства-члена  (НСПТ отправителя сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "\\d{3}", maxLength = 3)
    private String countryCode;
    
    @Documentation("Число полученное на предыдущем запросе, для получения только новых сообщений")
    @XmlElement(required = true)
    private long since;

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocDateTime() {
        return docDateTime;
    }

    public void setDocDateTime(String docDateTime) {
        this.docDateTime = docDateTime;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public long getSince() {
        return since;
    }

    public void setSince(long since) {
        this.since = since;
    }

    @Override
    public String toString() {
        return "QueryUpdateParticipantsRequest [docId=" + docId + ", docDateTime=" + docDateTime + ", countryCode=" + countryCode + ", since=" + since + "]";
    }

}
