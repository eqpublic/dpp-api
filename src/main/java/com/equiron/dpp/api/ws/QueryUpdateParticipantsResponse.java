package com.equiron.dpp.api.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.entity.Participant;
import com.equiron.dpp.common.model.ws.Error;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Сведения о новых участниках")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseQueryUpdateParticipants")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class QueryUpdateParticipantsResponse {
    @Documentation("Идентификатор электронного документа (сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docId;
    
    @Documentation("Идентификатор электронного документа (сведений), в ответ на который был сформирован данный электронный документ (сведения)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docRefId;

    @Documentation("Дата и время создания обновлений классификатора товаров ЕАЭС")
    @XmlElement(required = true)
    @MinOccurs(1)
    @XmlSchemaType(name = "dateTime")
    //XMLGregorianCalendar docDateTime;
    private String docDateTime;
    
    @Documentation("Код государства-члена  (НСПТ отправителя сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "\\d{3}", maxLength = 3)
    private String countryCode;
    
    @Documentation("Число полученное на предыдущем запросе, для получения только новых сообщений")
    @XmlElement(required = true)
    private long since;
    
    @Documentation("Список участников")
    @XmlElement(required = false, name = "participant")
    @MinOccurs(0)
    @XmlElementWrapper()
    private List<Participant> participants;

    @Documentation("Список ошибок в случае отклонения, необязательный параметр")
    @XmlElementWrapper
    @XmlElement(name = "error", required = false)
    @MinOccurs(0)
    private List<Error> errors;
    
    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocRefId() {
        return docRefId;
    }

    public void setDocRefId(String docRefId) {
        this.docRefId = docRefId;
    }

    public String getDocDateTime() {
        return docDateTime;
    }

    public void setDocDateTime(String docDateTime) {
        this.docDateTime = docDateTime;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public long getSince() {
        return since;
    }

    public void setSince(long since) {
        this.since = since;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "QueryUpdateParticipantsResponse [docId=" + docId + ", docRefId=" + docRefId + ", docDateTime=" + docDateTime + ", countryCode=" + countryCode
                + ", since=" + since + ", participants=" + participants + ", errors=" + errors + "]";
    }

}
