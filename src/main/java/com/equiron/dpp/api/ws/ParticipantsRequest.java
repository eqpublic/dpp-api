package com.equiron.dpp.api.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.Facets;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.entity.Participant;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Сведения об участниках оборота государства-члена ")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantsInformation")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
@XmlRootElement
public class ParticipantsRequest {
    @Documentation("Идентификатор электронного документа (сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", maxLength = 50)
    private String docId;
    
    @Documentation("Дата и время создания обновлений классификатора товаров ЕАЭС")
    @XmlElement(required = true)
    @MinOccurs(1)
    @XmlSchemaType(name = "dateTime")
    private String docDateTime;
    
    @Documentation("Код государства-члена  (НСПТ отправителя сведений)")
    @XmlElement(required = true)
    @MinOccurs(1)
    @Facets(pattern = "\\d{3}", maxLength = 3)
    private String countryCode;
    
    @Documentation("Список участников")
    @XmlElement(required = true, name = "participant")
    @MinOccurs(1)
    @XmlElementWrapper()
    private List<Participant> participants;
    
    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocDateTime() {
        return docDateTime;
    }

    public void setDocDateTime(String docDateTime) {
        this.docDateTime = docDateTime;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    @Override
    public String toString() {
        return "ParticipantsRequest [docId=" + docId + ", docDateTime=" + docDateTime + ", countryCode=" + countryCode + ", participants=" + participants + "]";
    }

}
