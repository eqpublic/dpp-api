package com.equiron.dpp.api.ws;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.MaxOccurs;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.ws.DeliveryConfirmationType;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Модель, представляющая подтверждения от НСПТ о полученных сведениях
 */
@Documentation("Реестр подтверждений")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Confirmation")
@XmlRootElement(namespace = "urn:dpp")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
@Entity
public class Confirmation {
    @Id
    @Documentation("Уникальный номер подтверждения")
    @MaxOccurs(1)
    @MinOccurs(1)
    private String id = UUID.randomUUID().toString();

    @Documentation("Статус сообщения")
    @XmlElement(name = "status", required = true)
    @Enumerated(EnumType.STRING)
    private DeliveryConfirmationType status;

    @Documentation("Код страны")
    @MaxOccurs(1)
    @MinOccurs(1)
    @XmlElement(required = true)
    private String nts;
    
    @XmlTransient
    @Documentation("оригинал документа")
    @MaxOccurs(0)
    @MinOccurs(0)
    private String doc;
    
    @Documentation("Дата и время получения подтверждения")
    @MaxOccurs(1)
    @MinOccurs(1)
    private Long timestamp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeliveryConfirmationType getStatus() {
        return status;
    }

    public void setStatus(DeliveryConfirmationType status) {
        this.status = status;
    }

    public String getNts() {
        return nts;
    }

    public void setNts(String nts) {
        this.nts = nts;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    
    @Override
    public String toString() {
        return "Confirmation [id=" + id + ", status=" + status + ", nts=" + nts + ", doc=" + doc + ", timestamp=" + timestamp + "]";
    }

  }
