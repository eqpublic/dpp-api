package com.equiron.dpp.api.ws;

import java.util.List;

import javax.xml.bind.annotation.Documentation;
import javax.xml.bind.annotation.MinOccurs;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.equiron.dpp.common.model.DeliveredType;
import com.equiron.dpp.common.model.ws.Error;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Documentation("Ответ на получение новых участников")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvideParticipantsResponse")
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class ProvideParticipantsResponse {
    @Documentation("Статус сообщения")
    @XmlElement(name = "status", required = true)
    @MinOccurs(1)
    private DeliveredType status;

    @Documentation("Список ошибок в случае отклонения, необязательный параметр")
    @XmlElementWrapper
    @XmlElement(name = "error", required = false)
    @MinOccurs(0)
    private List<Error> errors;
    
    public ProvideParticipantsResponse(DeliveredType delivered) {
        this.status = delivered;
    }

    public ProvideParticipantsResponse(DeliveredType validationError, List<Error> of) {
        this.status = validationError;
        this.errors = of;
    }

    public DeliveredType getStatus() {
        return status;
    }

    public void setStatus(DeliveredType status) {
        this.status = status;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ProvideParticipantsResponse [status=" + status + ", errors=" + errors + "]";
    }
    
}
