package com.equiron.dpp.api.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.validation.Valid;

import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.annotations.WSDLDocumentation;

import com.equiron.dpp.common.model.ws.DocumentsRequest;
import com.equiron.dpp.common.model.ws.DocumentsResponse;
import com.equiron.dpp.common.model.ws.UpdatesRequest;
import com.equiron.dpp.common.model.ws.UpdatesResponse;

@WebService(serviceName = "TransitionService", targetNamespace = "urn:dpp")
@SOAPBinding(parameterStyle = ParameterStyle.BARE)
@SchemaValidation
@WSDLDocumentation("Сервис для передачи сведений между несколькими НСПТ")
public interface TransitionService {
    @WSDLDocumentation("Метод для передачи первичного документа или подтверждений")
    @WebResult(name="documentsResponse")
    DocumentsResponse sendDocuments(@Valid @WebParam(name = "documentsRequest") DocumentsRequest documentsRequest) throws Exception;

    @WSDLDocumentation("Метод для получения новых сведений")
    @WebResult(name="updatesResponse")
    UpdatesResponse queryUpdates(@Valid @WebParam(name = "updatesRequest") UpdatesRequest updatesRequest) throws Exception;
}
