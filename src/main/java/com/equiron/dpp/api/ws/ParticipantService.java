package com.equiron.dpp.api.ws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.validation.Valid;

import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.annotations.SchemaValidation.SchemaValidationType;
import org.apache.cxf.annotations.WSDLDocumentation;

import com.equiron.dpp.common.model.entity.ConfirmRequest;
import com.equiron.dpp.common.model.entity.ConfirmResponse;
import com.equiron.dpp.common.model.ws.DocumentsResponse;
import com.equiron.dpp.common.model.ws.ParticipantDocumentRequest;
import com.equiron.dpp.common.model.ws.UpdatesRequest;
import com.equiron.dpp.common.model.ws.UpdatesResponse;

@WebService(serviceName = "ParticipantService", targetNamespace = "urn:dpp")
@SOAPBinding(parameterStyle = ParameterStyle.BARE)
@SchemaValidation(type = SchemaValidationType.BOTH)
@WSDLDocumentation("Сервис для передачи и приема реестра участников товарооборота")
public interface ParticipantService {
    @WSDLDocumentation("Метод для передачи реестра участников")
    @WebResult(name="documentsResponse")
    DocumentsResponse sendDocuments(@Valid @WebParam(name = "participantDocumentRequest") ParticipantDocumentRequest documentsRequest) throws Exception;

    @WSDLDocumentation("Метод для получения реестра участников")
    @WebResult(name="updatesResponse")
    UpdatesResponse queryUpdates(@Valid @WebParam(name = "updatesRequest") UpdatesRequest updatesRequest) throws Exception;
    
    @WSDLDocumentation("Метод для передачи в ЦПТ подтверждения о приеме участников товарооборота")
    @WebResult(name="catalogProductResponse")
    ConfirmResponse sendConfirm(@Valid @WebParam(name = "confirmRequest") ConfirmRequest confirmRequest) throws Exception;
    
}
