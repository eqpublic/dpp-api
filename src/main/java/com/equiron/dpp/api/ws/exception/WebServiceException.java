package com.equiron.dpp.api.ws.exception;

import javax.xml.ws.WebFault;

@WebFault(name = "webServiceException")
public class WebServiceException extends Exception {
    private static final long serialVersionUID = 1L;
    private String errCode = "-1";
    
    public WebServiceException(String message, String errCode) {
        super(message);
        this.errCode = errCode;
    }
    
    public WebServiceException(String message) {
        super(message);
    }
    
    public String getErrCode() {
        return errCode;
    }
}
