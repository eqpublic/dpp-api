package com.equiron.dpp.api.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.validation.Valid;

import org.apache.cxf.annotations.SchemaValidation;
import org.apache.cxf.annotations.SchemaValidation.SchemaValidationType;
import org.apache.cxf.annotations.WSDLDocumentation;

@WebService(serviceName = "ParticipantService", targetNamespace = "urn:dpp", name = "ParticipantService")
@SOAPBinding(parameterStyle = ParameterStyle.BARE)
@SchemaValidation(type = SchemaValidationType.BOTH)
@WSDLDocumentation("Сервис для передачи и приема реестра участников товарооборота")
public interface ParticipantServiceDpp {
    @WSDLDocumentation("Метод для передачи реестра участников в ЦПТ")
    @WebResult(name="participantsResponse")
    ProvideParticipantsResponse provideParticipants(@Valid @WebParam(name = "participantsRequest") ParticipantsRequest participantsRequest) throws Exception;

    @WSDLDocumentation("Метод для получения подтверждения о приемке ЦПТ сведений об участниках")
    @WebResult(name="queryConfirmationResponse")
    QueryConfirmationResponse queryConfirmation(@Valid @WebParam(name = "queryConfirmationRequest") QueryConfirmationRequest queryConfirmationRequest) throws Exception;

    @WSDLDocumentation("Метод для получения реестра участников")
    @WebResult(name="queryUpdatesResponse")
    @WebMethod(operationName = "queryUpdate")
    QueryUpdateParticipantsResponse queryUpdatesParticipants(@Valid @WebParam(name = "queryUpdateRequest") QueryUpdateParticipantsRequest queryParticipantsRequest) throws Exception;
    
    @WSDLDocumentation("Метод представления подтверждения о получении реестра участников ЕАЭС")
    @WebResult(name="provideConfirmationResponse")
    ProvideConfirmationResponse provideConfirmation(@Valid @WebParam(name = "provideConfirmationRequest") ProvideConfirmationRequest request) throws Exception;

}
