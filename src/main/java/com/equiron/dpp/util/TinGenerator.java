package com.equiron.dpp.util;

import java.util.Random;

public class TinGenerator {
    public static String generateTin() {
        StringBuilder sb = new StringBuilder(((Math.abs(new Random().nextLong()) + 100000000000L) + "").substring(0, 11));

        sb.replace(4, 4, "9");//for bin

        return sb.toString();
    }
}

