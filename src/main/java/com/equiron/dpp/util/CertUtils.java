package com.equiron.dpp.util;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Map;
import java.util.stream.Collectors;

import javax.security.auth.x500.X500Principal;
import javax.security.auth.x500.X500PrivateCredential;

public class CertUtils {
    public static X509Certificate loadFromString(String base64Encoded) throws CertificateParsingException {
        X509Certificate cert = null;
        
        try {
            cert = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(removeNewLines(base64Encoded))));
        } catch (Exception e) {
            throw new CertificateParsingException(e);
        }

        if (cert == null) throw new CertificateParsingException();

        return cert;
    }
    
    public static X500PrivateCredential loadCredentialFromResources(String path, String password) {
        try {
            KeyStore keyStore = KeyStore.getInstance("pkcs12");
            
            try (InputStream in = CertUtils.class.getResourceAsStream(path)) {
                return loadCredentialFromStream(password, keyStore, in);
            } 
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public static X500PrivateCredential loadCredentialFromStream(String password, KeyStore keyStore, InputStream in) {
        try {
            keyStore.load(in, password.toCharArray());
            
            Enumeration<String> aliases = keyStore.aliases();
            
            while (aliases.hasMoreElements()){
                String alias = aliases.nextElement();
                
                return new X500PrivateCredential((X509Certificate)keyStore.getCertificate(alias), 
                                                      (PrivateKey)keyStore.getKey(alias, password.toCharArray()));
            }
            
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public static X500PrivateCredential loadCredentialFromFile(String path, String password) {
        try {
            KeyStore keyStore = KeyStore.getInstance("pkcs12");
            
            try (InputStream in = new FileInputStream(path)) {
                return loadCredentialFromStream(password, keyStore, in);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private static String removeNewLines(String s) {
        return s.replace("\r", "").replace("\n", "").replace(" ", "");
    }
    
    public static String toBase64(X509Certificate certificate) throws CertificateEncodingException {
        return new String(Base64.getEncoder().encode(certificate.getEncoded()));
    }
    
    public static Map<String, String> parseSubject(X500Principal subjectX500Principal) {
        return Arrays.stream(subjectX500Principal.toString().split(",")).collect(Collectors.toMap(v -> v.trim().split("=")[0], v -> v.trim().split("=")[1]));
    }
    
    
//    public static void main(String[] args) {
//        X509Certificate cert = loadCredentialFromFile("c:\\1\\AUTH_RSA256.p12", "Qwerty12").getCertificate();
//        
//        X500Principal subjectX500Principal = cert.getSubjectX500Principal();
//        
//        Map<String, String> map = Arrays.stream(subjectX500Principal.toString().split(",")).collect(Collectors.toMap(v -> v.trim().split("=")[0], v -> v.trim().split("=")[1]));
//        
//        System.out.println(map.get("SERIALNUMBER"));
//    }
}
