INSERT INTO class_levels
(uid, id, code, name, status, version, parent_id, from_classifier, owner, date_created, user_created, operation, level)
VALUES('1f5a68b8-63cd-421d-91dd-11adbaaeacf6', 'c63f2aa2-31fa-4c12-a72e-7d5d32a01b07', '024', 'ТАБАК И ПРОМЫШЛЕННЫЕ ЗАМЕНИТЕЛИ ТАБАКА', 'PUBLISH', 1, NULL, 1, 'DPP', 1548138274, '00001111222233334444555566667777', 'ADD',1);
INSERT INTO class_levels
(uid, id, code, name, status, version, parent_id, from_classifier, owner, date_created, user_created, operation, level)
VALUES('f0eeec37-ecac-43f1-a14c-cfdaa8b6ddf7', '158baf59-fc31-4c0c-b57f-58c5306a26ae', '120', 'Сигары, сигары с обрезанными концами, сигариллы и сигареты, папиросы, биди, кретек из табака или заменителей табака', 'PUBLISH', 1, 'c63f2aa2-31fa-4c12-a72e-7d5d32a01b07', 1, 'DPP', 1548138287, '00001111222233334444555566667777', 'ADD', 2);




INSERT INTO class_blocks
(uid, id, code, "name", version, parent_id, date_created, user_created, "comment", code_gdsn, name_gdsn, note_gdsn, status, attribute_ids, tn_veds, from_classifier, owner, operation)
VALUES('0c209704-779c-45ae-b0d9-700a9dba14c0', 'f39bea80-9897-42c8-af60-44542eaadbf2', '006', 'Биди', 1, '158baf59-fc31-4c0c-b57f-58c5306a26ae', 1548138378, '00001111222233334444555566667777', NULL, NULL, NULL, NULL, 'PUBLISH', NULL, '240290', 1, 'DPP', 'ADD');
INSERT INTO class_blocks
(uid, id, code, "name", version, parent_id, date_created, user_created, "comment", code_gdsn, name_gdsn, note_gdsn, status, attribute_ids, tn_veds, from_classifier, owner, operation)
VALUES('901dcb17-b2dc-4fe7-90fb-47b8e838dcf8', 'ec4d9bea-5611-4290-b693-3afc4a5ef2a4', '003', 'Сигариллы (сигары тонкие)', 1, '158baf59-fc31-4c0c-b57f-58c5306a26ae', 1548138325, '00001111222233334444555566667777', NULL, NULL, NULL, NULL, 'PUBLISH', NULL, '240210', 1, 'DPP', 'ADD');
INSERT INTO class_blocks
(uid, id, code, "name", version, parent_id, date_created, user_created, "comment", code_gdsn, name_gdsn, note_gdsn, status, attribute_ids, tn_veds, from_classifier, owner, operation)
VALUES('ac52f24c-ec48-474f-90ea-1e4d2642d0be', '4c0cef0e-ba7e-4ab9-8de3-122c798c2f64', '004', 'Сигареты, содержащие табак', 1, '158baf59-fc31-4c0c-b57f-58c5306a26ae', 1548138345, '00001111222233334444555566667777', NULL, NULL, NULL, NULL, 'PUBLISH', NULL, '240220', 1, 'DPP', 'ADD');
INSERT INTO class_blocks
(uid, id, code, "name", version, parent_id, date_created, user_created, "comment", code_gdsn, name_gdsn, note_gdsn, status, attribute_ids, tn_veds, from_classifier, owner, operation)
VALUES('aef6824a-5ec7-48c2-9d18-b799a5ea85f5', '9aba2ee4-87d3-4ca8-9134-669ae7fb92ca', '007', 'Кретек', 1, '158baf59-fc31-4c0c-b57f-58c5306a26ae', 1548138398, '00001111222233334444555566667777', NULL, NULL, NULL, NULL, 'PUBLISH', NULL, '240290', 1, 'DPP', 'ADD');
INSERT INTO class_blocks
(uid, id, code, "name", version, parent_id, date_created, user_created, "comment", code_gdsn, name_gdsn, note_gdsn, status, attribute_ids, tn_veds, from_classifier, owner, operation)
VALUES('cad1d716-010f-489c-b665-af6181d71a6e', 'df02dfb2-e6d0-4c5e-8f48-1f1d6ad3fed0', '005', 'Папиросы', 1, '158baf59-fc31-4c0c-b57f-58c5306a26ae', 1548138362, '00001111222233334444555566667777', NULL, NULL, NULL, NULL, 'PUBLISH', NULL, '240290', 1, 'DPP', 'ADD');
INSERT INTO class_blocks
(uid, id, code, "name", version, parent_id, date_created, user_created, "comment", code_gdsn, name_gdsn, note_gdsn, status, attribute_ids, tn_veds, from_classifier, owner, operation)
VALUES('d4dce4f3-23f5-4330-b2fa-e7d7fd95ab32', '0e5ffcc1-f63f-44e8-942b-63440368606a', '135', 'Сигары, сигары с обрезанными концами (черуты)', 1, '158baf59-fc31-4c0c-b57f-58c5306a26ae', 1548138306, '00001111222233334444555566667777', NULL, NULL, NULL, NULL, 'PUBLISH', NULL, '240210', 1, 'DPP', 'ADD');


INSERT INTO classifier_diffs
(uid, id, classes, blocks, "attributes", version, date_created, country_send, country_confirm, owner, status, dictionaries)
VALUES('d2245ddf-66e6-4ae1-baa0-1e07dababf8c', '31b53e09-0118-4625-b650-7ef96e4416f6', '[{"uid":"1f5a68b8-63cd-421d-91dd-11adbaaeacf6","id":"c63f2aa2-31fa-4c12-a72e-7d5d32a01b07","code":"024","name":"ТАБАК И ПРОМЫШЛЕННЫЕ ЗАМЕНИТЕЛИ ТАБАКА","version":1,"dateCreated":1548138274,"userCreated":"00001111222233334444555566667777","status":"PUBLISH","fromClassifier":1,"owner":"DPP","operation":"ADD"},{"uid":"f0eeec37-ecac-43f1-a14c-cfdaa8b6ddf7","id":"158baf59-fc31-4c0c-b57f-58c5306a26ae","code":"120","name":"Сигары, сигары с обрезанными концами, сигариллы и сигареты, папиросы, биди, кретек из табака или заменителей табака","version":1,"dateCreated":1548138287,"userCreated":"00001111222233334444555566667777","status":"PUBLISH","parentId":"c63f2aa2-31fa-4c12-a72e-7d5d32a01b07","fromClassifier":1,"owner":"DPP","operation":"ADD"}]', '[{"uid":"0c209704-779c-45ae-b0d9-700a9dba14c0","id":"f39bea80-9897-42c8-af60-44542eaadbf2","code":"006","name":"Биди","version":1,"parentId":"158baf59-fc31-4c0c-b57f-58c5306a26ae","dateCreated":1548138378,"userCreated":"00001111222233334444555566667777","fromClassifier":1,"owner":"DPP","status":"PUBLISH","tnVeds":"240290","operation":"ADD"},{"uid":"901dcb17-b2dc-4fe7-90fb-47b8e838dcf8","id":"ec4d9bea-5611-4290-b693-3afc4a5ef2a4","code":"003","name":"Сигариллы (сигары тонкие)","version":1,"parentId":"158baf59-fc31-4c0c-b57f-58c5306a26ae","dateCreated":1548138325,"userCreated":"00001111222233334444555566667777","fromClassifier":1,"owner":"DPP","status":"PUBLISH","tnVeds":"240210","operation":"ADD"},{"uid":"ac52f24c-ec48-474f-90ea-1e4d2642d0be","id":"4c0cef0e-ba7e-4ab9-8de3-122c798c2f64","code":"004","name":"Сигареты, содержащие табак","version":1,"parentId":"158baf59-fc31-4c0c-b57f-58c5306a26ae","dateCreated":1548138345,"userCreated":"00001111222233334444555566667777","fromClassifier":1,"owner":"DPP","status":"PUBLISH","tnVeds":"240220","operation":"ADD"},{"uid":"aef6824a-5ec7-48c2-9d18-b799a5ea85f5","id":"9aba2ee4-87d3-4ca8-9134-669ae7fb92ca","code":"007","name":"Кретек","version":1,"parentId":"158baf59-fc31-4c0c-b57f-58c5306a26ae","dateCreated":1548138398,"userCreated":"00001111222233334444555566667777","fromClassifier":1,"owner":"DPP","status":"PUBLISH","tnVeds":"240290","operation":"ADD"},{"uid":"cad1d716-010f-489c-b665-af6181d71a6e","id":"df02dfb2-e6d0-4c5e-8f48-1f1d6ad3fed0","code":"005","name":"Папиросы","version":1,"parentId":"158baf59-fc31-4c0c-b57f-58c5306a26ae","dateCreated":1548138362,"userCreated":"00001111222233334444555566667777","fromClassifier":1,"owner":"DPP","status":"PUBLISH","tnVeds":"240290","operation":"ADD"},{"uid":"d4dce4f3-23f5-4330-b2fa-e7d7fd95ab32","id":"0e5ffcc1-f63f-44e8-942b-63440368606a","code":"135","name":"Сигары, сигары с обрезанными концами (черуты)","version":1,"parentId":"158baf59-fc31-4c0c-b57f-58c5306a26ae","dateCreated":1548138306,"userCreated":"00001111222233334444555566667777","fromClassifier":1,"owner":"DPP","status":"PUBLISH","tnVeds":"240210","operation":"ADD"}]', '[]', 1, 1548138401, '398,051', '643', 'DPP', 'INWORK', NULL);



INSERT INTO classifier_dpps
(id, doc, version, "timestamp", date_created, date_changed, draft_user_uid, last_change_user_uid, date_publish, date_finish, status, owner, id_clases)
VALUES('93fb20aa-77f5-4b57-a565-2dd8c5a11f6c', NULL, 1, NULL, 1548138251, 1548138398, 'admin@test', '00001111222233334444555566667777', 1548138401, NULL, 'PART_PUBLISH', 'DPP', NULL);
    